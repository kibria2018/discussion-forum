# forum/urls.py
from django.urls import path
from .views import forum_home

app_name = 'forum'

urlpatterns = [
    path('', forum_home, name='forum_home'),
]
