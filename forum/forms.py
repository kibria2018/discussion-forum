# forum/forms.py
from django import forms
from .models import Reply  # Make sure to import the Reply model

class TopicForm(forms.Form):
    title = forms.CharField(max_length=255, label='Title')
    content = forms.CharField(widget=forms.Textarea, label='Content')
    created_by = forms.CharField(max_length=255, label='Your Name')

class ReplyForm(forms.ModelForm):
    class Meta:
        model = Reply
        fields = ['content', 'created_by', 'parent_reply']

    parent_reply_id = forms.IntegerField(widget=forms.HiddenInput(), required=False)