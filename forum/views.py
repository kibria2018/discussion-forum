# forum/views.py
from django.db.models import Q
from django.http import HttpResponseServerError
from django.shortcuts import render, redirect, get_object_or_404
from .models import Topic, Reply
from .forms import TopicForm, ReplyForm

def forum_home(request, topic_id=None):
    try:
        # Retrieve all topics, ordered by submission time (newest first)
        topics = Topic.objects.all().order_by('-created_at')

        # Handle search queries
        search_query = request.GET.get('q', '')
        if search_query:
            topics = topics.filter(Q(title__icontains=search_query) | Q(content__icontains=search_query) |
                                   Q(replies__content__icontains=search_query))

        if request.method == 'POST':
            # Handle form submissions

            if 'topic_id' in request.POST:
                # Handle reply form submission
                topic_id = request.POST['topic_id']
                topic = get_object_or_404(Topic, id=topic_id)
                form = ReplyForm(request.POST)
                if form.is_valid():
                    parent_reply_id = form.cleaned_data.get('parent_reply_id')
                    parent_reply = None
                    if parent_reply_id:
                        parent_reply = get_object_or_404(Reply, id=parent_reply_id)
                    reply = Reply(
                        content=form.cleaned_data['content'],
                        created_by=form.cleaned_data['created_by'],
                        topic=topic,
                        parent_reply=parent_reply
                    )
                    reply.save()
            else:
                # Handle topic form submission
                form = TopicForm(request.POST)
                if form.is_valid():
                    topic = Topic(
                        title=form.cleaned_data['title'],
                        content=form.cleaned_data['content'],
                        created_by=form.cleaned_data['created_by']
                    )
                    topic.save()
                    # Redirect to the forum home page after creating the topic
                    return redirect('forum:forum_home')
        else:
            form = TopicForm()

        return render(request, 'forum_home.html', {'form': form, 'topics': topics})
    except Exception as e:
        # Handle exceptions
        error_message = f"An error occurred: {str(e)}"
        return HttpResponseServerError(error_message)
