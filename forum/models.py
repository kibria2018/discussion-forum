# forum/models.py
from django.db import models

class Topic(models.Model):
    title = models.CharField(max_length=255)
    content = models.TextField()
    created_by = models.CharField(max_length=255)  # Change this line
    created_at = models.DateTimeField(auto_now_add=True)

class Reply(models.Model):
    content = models.TextField()
    created_by = models.CharField(max_length=255)  # Change this line
    created_at = models.DateTimeField(auto_now_add=True)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE, related_name='replies')
    parent_reply = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE, related_name='replies')

    def __str__(self):
        return f'Reply by {self.created_by} on {self.created_at}'