# Discussion Forum

This is a simple discussion forum web application built using Django, SQLite, and jQuery.

## Getting Started

### Prerequisites

- Python (3.x)
- Django (3.x)

### Installation

1. Clone the repository:

    ```bash
    git clone https://github.com/your-username/discussion-forum.git
    ```

2. Change into the project directory:

    ```bash
    cd discussion-forum
    ```

3. Create a virtual environment (optional but recommended):

    ```bash
    python -m venv venv
    ```

4. Activate the virtual environment:

    - On Windows:

        ```bash
        venv\Scripts\activate
        ```

    - On macOS and Linux:

        ```bash
        source venv/bin/activate
        ```

5. Install the dependencies:

    ```bash
    pip install -r requirements.txt
    ```

6. Apply database migrations:

    ```bash
    python manage.py migrate
    ```

7. Create a superuser for the admin interface (optional):

    ```bash
    python manage.py createsuperuser
    ```

8. Run the development server:

    ```bash
    python manage.py runserver
    ```

9. Open your web browser and visit [http://127.0.0.1:8000/forum/](http://127.0.0.1:8000/forum/) to view the discussion forum.

## Usage

- Create a new topic by clicking on the "Create Post" section on the left.
- Reply to a topic by using the reply form under each topic.
- Search for posts and their replies using the search form.

## Admin Interface

- Access the admin interface at [http://127.0.0.1:8000/admin/](http://127.0.0.1:8000/admin/) (if you created a superuser).
- Use the admin interface to manage topics, replies, and other models.

## Contributing

Feel free to contribute to this project by opening issues or creating pull requests. Your feedback and suggestions are welcome!

